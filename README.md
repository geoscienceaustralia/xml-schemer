## About

XML Schemer is a Xerces-J CLI wrapper for catalog-aware XSD validation.

## Usage

```
schemer.sh [--catalog <oasis-catalog-file>] --xml <xml-file> --xsd <xsd-file>
    --catalog <oasis-catalog-file>   schema catalog file
    --xml <xml-file>                 XML file
    --xsd <xsd-file>                 XSD file
```

## Contact Information

Contributions, suggestions, and bug reports are welcome!

Please feel free to contact us through Bitbucket or directly via email at gnss@ga.gov.au.
