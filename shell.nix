{
  pkgs ? import ./nix {},
  installDevTools ? true
}:

let
  projectName = "xml-schemer";

  buildTools = with pkgs; [
    awscli
    cacert
    docker
    git
    maven3
    openjdk8
  ];

  devTools = with pkgs; [
    niv
  ];

  env = pkgs.buildEnv {
    name = projectName + "-env";
    paths = buildTools ++ (
      if installDevTools then devTools else []
    );
  };

in
  pkgs.mkShell {
    buildInputs = [
      env
    ];
    shellHook = ''
      export PROJECT_NAME=${projectName}
      export SSL_CERT_FILE="${pkgs.cacert}/etc/ssl/certs/ca-bundle.crt"
      if [ -e "./aws-env.sh" ]; then
        . ./aws-env.sh
      fi
      : ''${AWS_ACCESS_KEY_ID:=$AWS_ACCESS_KEY_ID_GNSS_COMMON_PROD}
      : ''${AWS_SECRET_ACCESS_KEY:=$AWS_SECRET_ACCESS_KEY_GNSS_COMMON_PROD}
      export AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY
    '';
  }
