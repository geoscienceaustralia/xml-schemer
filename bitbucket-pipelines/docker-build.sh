#!/usr/bin/env bash

set -euo pipefail

organization=geoscienceaustralia
repository=${PROJECT_NAME}-bitbucket-pipelines
username=geodesyarchive
push=false

while [[ $# -gt 0 ]]; do
    case $1 in
        -p|--push)
            push=true
            shift
            ;;
        *)
            echo "Unknown option: $1"
            exit 1
            ;;
    esac
done

cd "$(dirname "${BASH_SOURCE[0]}")"

docker build -t "$organization/$repository" -f Dockerfile ..

if [[ $push = "true" ]]; then
    aws ssm get-parameter --name "/dockerhub/user/$username/password" \
        --with-decryption --query Parameter.Value --output text |
        docker login -u $username --password-stdin

    docker push "$organization/$repository"
fi
